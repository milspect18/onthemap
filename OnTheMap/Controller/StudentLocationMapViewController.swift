//
//  StudentLocationMapViewController.swift
//  OnTheMap
//
//  Created by Kyle Price on 4/29/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//

import UIKit
import MapKit

class StudentLocationMapViewController: UIViewController, MKMapViewDelegate {
    // MARK: Properties
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingText: UILabel!
    
    var mkPoints = [MKPointAnnotation]()

    
    // MARK: Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadPins()
    }
    
    
    // Grab all of the pins from Udacity then load then to the MapView
    private func loadPins() {
        mapView.removeAnnotations(mkPoints)
        loadStudentData(with: DataModel.studentData)
        mapView.addAnnotations(mkPoints)
        loadingView.isHidden = true
    }
    
    
    
    private func loadStudentData(with students: [StudentLocation]) {
        mkPoints = [MKPointAnnotation]()    // zero the array
        
        for student in students {
            let lat = CLLocationDegrees(student.latitude ?? 0.0)
            let lon = CLLocationDegrees(student.longitude ?? 0.0)
            let studentAnn = MKPointAnnotation()
            studentAnn.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            studentAnn.title = "\(student.firstName ?? "") \(student.lastName ?? "")"
            studentAnn.subtitle = student.mediaURL ?? ""
            
            self.mkPoints.append(studentAnn)
        }
    }
    
    
    // MARK: MapViewDelegate methods
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        let reuseId = "pin"
        
        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as? MKPinAnnotationView
        
        if pinView == nil {
            pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView!.canShowCallout = true
            pinView!.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        } else {
            pinView!.annotation = annotation
        }
        
        return pinView
    }
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        if control == view.rightCalloutAccessoryView {
            guard
                let urlForPin = view.annotation?.subtitle,
                let loweredUrl = urlForPin?.lowercased()
            else {
                return
            }
            
            let hasHttp = loweredUrl.contains("http://") || loweredUrl.contains("https://")
            let toOpen = hasHttp ? loweredUrl : "http://\(loweredUrl)"
            
            if let url = URL(string: toOpen) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
    
    // MARK: Actions
    @IBAction func addButtonPressed() {
        if DataModel.curStudentIdx != nil {
            let title = "Previous Location Exists"
            let message = "There is an existing location for this user, would you like to overwrite it?"
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Overwrite", style: .default) { (sender) in
                // Load and present the update screen
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddUpdateViewController") as! UINavigationController
                self.present(vc, animated: true, completion: nil)
            })
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            isLoading(true)
            var newStudent = StudentLocation()
            newStudent.firstName = DataModel.udacityFirstName
            newStudent.lastName = DataModel.udacityLastName
            ParseClient.shared.postNewStudentLocation(for: newStudent) { (loc, error) in
                if let error = error as? ParseClient.ParseError {
                    switch(error) {
                    case .JSONParsingError(let message):
                        self.updateLoadingText(with: message)
                    case .JSONBodyCreationError(let message):
                        self.updateLoadingText(with: message)
                    case .UnknownError(let err):
                        self.updateLoadingText(with: err.localizedDescription)
                    default:
                        self.updateLoadingText(with: "Error occured")
                    }
                    
                    self.isLoading(false)
                }
                
                if let loc = loc {
                    DataModel.curStudentIdx = 0
                    DataModel.studentData.insert(loc, at: DataModel.curStudentIdx!)
                    
                    // Load and present the update screen
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddUpdateViewController") as! UINavigationController
                    self.isLoading(false)
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    @IBAction func logoutButtonPressed() {
        UdacityClient.shared.logoutOfCurrentSession() { error in
            if let error = error as? UdacityClient.UdacityError {
                switch(error) {
                case .UnknownError:
                    self.goToLogin()
                case .JSONParsingError:
                    self.goToLogin()
                default:
                    self.goToLogin()
                }
                
                return
            }
            
            self.goToLogin()
        }
    }
    
    
    private func goToLogin() {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    private func isLoading(_ state: Bool) {
        if state {
            DispatchQueue.main.async {
                self.loadingView.isHidden = !state
                UIView.animate(withDuration: 0.35, animations: {
                    self.loadingView.alpha = 1.0
                })
            }
        } else {
            DispatchQueue.main.async {
                self.loadingView.isHidden = state
                UIView.animate(withDuration: 0.35, animations: {
                    self.loadingView.alpha = 0.0
                })
            }
        }
    }
    
    
    private func updateLoadingText(with text: String) {
        DispatchQueue.main.async {
            self.loadingText.text = text
        }
    }
}
