//
//  LogInViewController.swift
//  OnTheMap
//
//  Created by Kyle Price on 4/21/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//

import UIKit

class LogInViewController: UIViewController {
    // Outlets for the UI objects as well as their constraints
    // animation is done by manipulating the constraints
    @IBOutlet weak var udacityLogo: UIImageView!
    @IBOutlet weak var logoWidth: NSLayoutConstraint!
    @IBOutlet weak var logoHeight: NSLayoutConstraint!
    @IBOutlet weak var logoYPos: NSLayoutConstraint!
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var usernameWidth: NSLayoutConstraint!
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordWidth: NSLayoutConstraint!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginButtonWidth: NSLayoutConstraint!
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var errorLabelWidth: NSLayoutConstraint!
    
    @IBOutlet weak var loadingBlackout: UIView!
    @IBOutlet weak var loadingText: UILabel!
    
    var screenWidth: CGFloat = 0
    var screenHeight: CGFloat = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        subscribeToKeyboardEvents()
        screenWidth = view.frame.width
        screenHeight = view.frame.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetUiElements()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        runAnimation()
    }
    
    
    // Kill the keyboard if the screen is tapped
    @IBAction func screenTapped() {
        usernameTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    
    @IBAction func loginPressed(_ sender: UIButton) {
        guard
            let uname = usernameTextField.text, uname != "",
            let pword = passwordTextField.text, pword != ""
        else {
            updateErrorText(to: "Must enter username and password")
            return
        }
        
        isLoading(true)
        
        // Read valid username and password string that contained text to
        // get to this point.
        UdacityClient.shared.authenticateSession(username: uname, password: pword) { (error) in
            if let error = error as? UdacityClient.UdacityError {
                switch error {
                case .JSONParsingError(let message):
                    self.updateErrorText(to: message)
                case .BadLoginError(let message):
                    self.updateErrorText(to: message)
                case .UdacitySessionGenerationError(let message):
                    self.updateErrorText(to: message)
                case .JSONBodyCreationError(let message):
                    self.updateErrorText(to: message)
                default:
                    self.updateErrorText(to: "Unable to authenticate")
                }
                
                self.isLoading(false)
                return
            }
            
            self.loadUserData()
        }
    }
    
    
    private func loadUserData() {
        isLoading(true)
        updateLoadingText(to: "Loading Student data...")
        ParseClient.shared.getAllStudentLocations(limit: 100, skip: 0, order: "-updatedAt") { (students, error) in
            if let error = error as? ParseClient.ParseError {
                switch(error) { // This ParseClient method only passes UnknownError or JSONParsingError
                case .UnknownError(let err):
                    self.updateErrorText(to: err.localizedDescription)
                case .JSONParsingError(let message):
                    self.updateErrorText(to: message)
                default:    // If somehow we magically get another error type back...
                    self.updateErrorText(to: "Error occured.  Unable to retreive students.")
                }
                
                self.isLoading(false)
                return
            }
            
            // If we got a valid list of StudentLocations, then we can update
            // the managed object data with the list of unique and valid students
            // this should de-clutter the list of junk an test inputs in the data set
            if let students = students {
                DataModel.studentData.append(contentsOf: self.uniqueLocationList(students))
                self.findCurrentUser()
            } else {
                return
            }
        }
    }
    
    
    private func findCurrentUser() {
        var searchKey = ""
        updateLoadingText(to: "Loading Current User...")
        
        let curSessionAccountKey = UdacityClient.shared.getCurrentSession().account.key!
        UdacityClient.shared.getUserFullName(withKey: curSessionAccountKey) { (first, last, key, error) in
            if let error = error as? UdacityClient.UdacityError {
                switch(error) {
                case .JSONParsingError(let message):
                    self.updateErrorText(to: message)
                default:
                    self.updateErrorText(to: "Error occured when getting current user data")
                }
                
                self.isLoading(false)
                return
            }
            
            DataModel.udacityFirstName = first ?? ""
            DataModel.udacityLastName = last ?? ""
            
            if let key = key {
                searchKey = "{\"uniqueKey\":\"\(key)\"}"
            } else if let first = first, let last = last {
                searchKey = "{\"firstName\":\"\(first)\",\"lastName\":\"\(last)\"}"
            }
            
            self.getCurrentStudentInfo(with: searchKey)
        }
    }
    
    private func getCurrentStudentInfo(with searchKey: String) {
        ParseClient.shared.getSingleStudentLocation(with: searchKey) { (locs, error) in
            if let error = error as? ParseClient.ParseError {
                switch error {
                case .NoResults(let message):
                    self.updateLoadingText(to: message)
                    self.isLoading(false)
                    self.launchMap()
                    return
                case .UnknownError(let err):
                    self.updateErrorText(to: err.localizedDescription)
                case .JSONParsingError(let message):
                    self.updateErrorText(to: message)
                default:
                    self.updateLoadingText(to: "Error occured, unable to load user.")
                }
                
                self.isLoading(false)
                return
            }
            
            // If there are multiple records, the last item in the array
            // returned is always the most recently updated
            if let latestRecord = locs?.last {
                self.updateCurrentStudentIndex(latestRecord)
            }
        }
    }
    
    private func updateCurrentStudentIndex(_ currentStudent: StudentLocation) {
        if let index = DataModel.studentData.index(of: currentStudent) {
            DataModel.curStudentIdx = index
        }
        
        isLoading(false)
        launchMap()
    }
    
    private func launchMap() {
        DispatchQueue.main.async {
            // Launch the mapview
            // No error returned so we successfully authenticated
            // Move along to the tab bar view
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarView") as? UITabBarController {
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
  
    private func uniqueLocationList(_ students: [StudentLocation]) -> [StudentLocation] {
        var uniqueLocationsArray = [String:StudentLocation]()
        
        for student in students {
            guard
                let first = student.firstName,
                let last = student.lastName,
                let uniqueKey = student.uniqueKey
            else {
                continue
            }

            if first.isEmpty || last.isEmpty {
                continue
            }
            
            if let prevStudentLoc = uniqueLocationsArray[uniqueKey] {   // We have seen this uniqueKey before
                uniqueLocationsArray[uniqueKey] = mostRecentStudentData(prevStudentLoc, student)
            } else {
                uniqueLocationsArray[uniqueKey] = student
            }
        }
        
        return Array(uniqueLocationsArray.values)
    }
    
    
    private func mostRecentStudentData(_ first: StudentLocation, _ second: StudentLocation) -> StudentLocation {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSz"
        var datesDict = [String:Date?]()
        
        datesDict["firstCreated"] = dateFormatter.date(from: (first.createdAt ?? ""))
        datesDict["firstUpdated"] = dateFormatter.date(from: (first.updatedAt ?? ""))
        datesDict["secondCreated"] = dateFormatter.date(from: (second.createdAt ?? ""))
        datesDict["secondUpdated"] = dateFormatter.date(from: (second.updatedAt ?? ""))
        
        
        // Trim any nil values from the dictionary -----
        let keysToRemove = datesDict.keys.filter {
            guard let value = datesDict[$0] else { return false }
            return value == nil
        }
        
        for key in keysToRemove {
            datesDict.removeValue(forKey: key)
        }   // -----------------------------------------
        
        var dateArray = Array(datesDict.values)
        dateArray = dateArray.sorted(by: { (first, second) -> Bool in
            first!.compare(second!) == .orderedDescending
        })
        
        for (key, value) in datesDict {
            if value! == dateArray[0]! {
                if key.contains("first") {
                    return first
                } else {
                    return second
                }
            }
        }
        
        // All tests failed, kick back the latest one found
        print("Got here")
        return second
    }
    
    
    private func runAnimation() {
        // Animate the logo then present the buttons
        UIView.animate(withDuration: 0.5, delay: 0.25, options: .transitionCurlUp, animations: {
            // Slide logo
            self.logoYPos.constant -= self.screenHeight * 0.25
            self.view.layoutIfNeeded()
        }) { (success) in
            // Display textfields and button
            self.showUserName()
        }
    }
    
    private func showUserName() {
        UIView.animate(withDuration: 0.15, animations: {
            self.usernameTextField.alpha = 1
            self.usernameWidth.constant = self.screenWidth * 0.75
            self.view.layoutIfNeeded()
        }) { (success) in
            self.showPassWord()
        }
    }
    
    private func showPassWord() {
        UIView.animate(withDuration: 0.15, animations: {
            self.passwordTextField.alpha = 1
            self.passwordWidth.constant = self.screenWidth * 0.75
            self.view.layoutIfNeeded()
        }) { (success) in
            self.showLoginButton()
        }
    }
    
    private func showLoginButton() {
        UIView.animate(withDuration: 0.15) {
            self.loginButton.alpha = 1
            self.loginButtonWidth.constant = self.screenWidth * 0.35
            self.errorLabel.alpha = 1.0
            self.view.layoutIfNeeded()
        }
    }
    
    private func resetUiElements() {
        errorLabel.alpha = 0.0
        errorLabelWidth.constant = screenWidth * 0.9
        usernameTextField.alpha = 0
        usernameWidth.constant = 0
        passwordTextField.alpha = 0
        passwordWidth.constant = 0
        loginButton.alpha = 0
        loginButtonWidth.constant = 0
        loadingBlackout.isHidden = true
        loadingBlackout.alpha = 0
        logoYPos.constant = 0
        usernameTextField.text = ""
        passwordTextField.text = ""
    }
    
    // Display the dark blur with loading animated icon
    // or remove it from display
    private func isLoading(_ loading: Bool) {
        DispatchQueue.main.async {
            if loading {
                UIView.animate(withDuration: 0.35, animations: {
                    self.loadingBlackout.isHidden = !loading
                    self.loadingBlackout.alpha = 1
                })
            } else {
                UIView.animate(withDuration: 0.35, animations: {
                    self.loadingBlackout.alpha = 0
                    self.loadingBlackout.isHidden = !loading
                })
            }
        }
    }
    
    
    private func updateLoadingText(to new: String) {
        DispatchQueue.main.async {
            self.loadingText.text = new
        }
    }
    
    private func updateErrorText(to new: String) {
        DispatchQueue.main.async {
            self.errorLabel.text = new
        }
    }
}


// MARK: Delegate for the text fields
//
// Since we aren't doing anything particular or different between
// the two text fields, both can share this delegation to hide the
// keyboard when the return/done key is pressed
extension LogInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.errorLabel.text = ""
    }
}



// MARK: Keyboard visual collision handling
extension LogInViewController {
    func subscribeToKeyboardEvents() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustViewForKeyboardShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustViewForKeyboardHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    func unsubscribeToKeyboardEvents() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func adjustViewForKeyboardShow(_ sender: Notification) {
        // First reset the view positon so we can accomodate changing
        // keyboards (i.e. from username keyboard to password keyboard)
        view.frame.origin.y = 0
        
        let viewHeight = view.frame.height
        let keyboardTopY = viewHeight - getKeyboardHeight(sender)
        let passTextHeight = passwordTextField.frame.height
        let passTextYPos = passwordTextField.frame.origin.y
        let passTextBottom = passTextYPos + passTextHeight
        
        // If the keyboard is overlapping the lower text field, then we need
        // to shift everything up
        if keyboardTopY < passTextBottom {
            let overlap = passTextBottom - keyboardTopY
            view.frame.origin.y -= overlap + 25
        }
    }
    
    // Shift the view back down when the keyboard hides
    @objc func adjustViewForKeyboardHide(_ sender: Notification) {
        view.frame.origin.y = 0
    }
    
    // Helper method to find the current height of the keyboard
    private func getKeyboardHeight(_ notification: Notification) -> CGFloat {
        let userInfo = notification.userInfo
        if let keyboardSize = userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            return keyboardSize.cgRectValue.height
        } else {
            return 0
        }
    }
}

