//
//  AddUpdateLocationViewController.swift
//  OnTheMap
//
//  Created by Kyle Price on 5/5/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//

import UIKit
import CoreLocation

class AddUpdateLocationViewController: UIViewController, UITextFieldDelegate {
   
    @IBOutlet weak var findOnMapButton: UIButton!
    @IBOutlet weak var blueFrameHeight: NSLayoutConstraint!
    @IBOutlet weak var locationText: UITextField!
    @IBOutlet weak var findBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var locatingView: UIVisualEffectView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUi()
    }

    @IBAction func findButtonPressed(_ sender: Any) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "updateMediaUrl") as? UpdateMediaUrlViewController {
            guard let mapString = locationText.text else { return }
            isLocating(true)
            CLGeocoder().geocodeAddressString(mapString) { (placemark, error) in
                if error != nil {
                    self.isLocating(false)
                    self.locationText.text = "Error finding location"
                    return
                }
                
                if let placemark = placemark?[0] {
                    guard let latitude = placemark.location?.coordinate.latitude else { return }
                    guard let longitude = placemark.location?.coordinate.longitude else { return }
                    let curStudentIdx = DataModel.curStudentIdx!
                    
                    DataModel.studentData[curStudentIdx].mapString = mapString
                    DataModel.studentData[curStudentIdx].latitude = Float(latitude)
                    DataModel.studentData[curStudentIdx].longitude = Float(longitude)
                    
                    vc.placemark = placemark
                    self.isLocating(false)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @IBAction func cancelBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func screenTapped() {
        locationText.resignFirstResponder()
    }
    
    
    private func setupUi() {
        locatingView.isHidden = true
        locatingView.alpha = 0.0
        blueFrameHeight.constant = view.frame.height * (1.0/3.0)
        findBtnWidth.constant = view.frame.width * 0.5
        findOnMapButton.layer.borderWidth = 2
        findOnMapButton.layer.cornerRadius = 10
        findOnMapButton.layer.borderColor = findOnMapButton.tintColor.cgColor
        locationText.delegate = self
        
        if let str = DataModel.studentData[DataModel.curStudentIdx!].mapString, str != "" {
            locationText.text = str
        } else {
            locationText.text = "Enter Location"
            disableBtn(true)
        }
    }
    
    
    private func disableBtn(_ state: Bool) {
        findOnMapButton.isEnabled = !state
        findOnMapButton.layer.borderColor = findOnMapButton.tintColor.cgColor
    }
    
    private func isLocating(_ state: Bool) {
        if state {
            DispatchQueue.main.async {
                self.locatingView.isHidden = !state
                UIView.animate(withDuration: 0.35, animations: {
                    self.locatingView.alpha = 1.0
                })
            }
        } else {
            DispatchQueue.main.async {
                self.locatingView.isHidden = state
                UIView.animate(withDuration: 0.35, animations: {
                    self.locatingView.alpha = 0.0
                })
            }
        }
    }
    
    
    // MARK: Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let locText = textField.text, locText != "" {
            findOnMapButton.isEnabled = true
        }
    }
}
