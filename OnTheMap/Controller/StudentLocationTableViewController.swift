//
//  StudentLocationTableViewController.swift
//  OnTheMap
//
//  Created by Kyle Price on 5/15/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//

import UIKit

class StudentLocationTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var studentListTableView: UITableView!
    @IBOutlet weak var loadingView: UIVisualEffectView!
    @IBOutlet weak var loadingText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        studentListTableView.delegate = self
        studentListTableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        studentListTableView.reloadData()
        loadingView.isHidden = true
        loadingView.alpha = 0.0
    }
    
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataModel.studentData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentLocTableViewCell", for: indexPath)
        
        // Configure the cell...
        let thisStudent = DataModel.studentData[indexPath.row]
        let nameText = "\(thisStudent.firstName ?? "") \(thisStudent.lastName ?? "")"
        let detailText = "\(thisStudent.mapString ?? "") - \(thisStudent.mediaURL ?? "")"
        
        cell.textLabel?.text = nameText
        cell.detailTextLabel?.text = detailText
        cell.imageView?.image = UIImage(named: "icon_pin")
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let thisStudent = DataModel.studentData[indexPath.row]
        var hasHttp = thisStudent.mediaURL?.lowercased().contains("http://") ?? false
        hasHttp = hasHttp || thisStudent.mediaURL?.lowercased().contains("https://") ?? false
        let toOpen = hasHttp ? thisStudent.mediaURL! : "http://\(thisStudent.mediaURL ?? "")"
        
        if let url = URL(string: toOpen) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    
    // MARK: Actions
    @IBAction func addButtonPressed() {
        if DataModel.curStudentIdx != nil {
            let title = "Previous Location Exists"
            let message = "There is an existing location for this user, would you like to overwrite it?"
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Overwrite", style: .default) { (sender) in
                // Load and present the update screen
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddUpdateViewController") as! UINavigationController
                self.present(vc, animated: true, completion: nil)
            })
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        } else {
            isLoading(true)
            var newStudent = StudentLocation()
            newStudent.firstName = DataModel.udacityFirstName
            newStudent.lastName = DataModel.udacityLastName
            ParseClient.shared.postNewStudentLocation(for: newStudent) { (loc, error) in
                if let error = error as? ParseClient.ParseError {
                    switch(error) {
                    case .JSONParsingError(let message):
                        self.updateLoadingText(with: message)
                    case .JSONBodyCreationError(let message):
                        self.updateLoadingText(with: message)
                    case .UnknownError(let err):
                        self.updateLoadingText(with: err.localizedDescription)
                    default:
                        self.updateLoadingText(with: "Error occured")
                    }
                    
                    self.isLoading(false)
                }
                
                if let loc = loc {
                    DataModel.curStudentIdx = 0
                    DataModel.studentData.insert(loc, at: DataModel.curStudentIdx!)
                    
                    // Load and present the update screen
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddUpdateViewController") as! UINavigationController
                    self.isLoading(false)
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    
    @IBAction func logoutButtonPressed() {
        UdacityClient.shared.logoutOfCurrentSession() { error in
            if let error = error as? UdacityClient.UdacityError {
                switch(error) {
                case .UnknownError(let err):
                    self.goToLogin(withErrorString: "\(err.localizedDescription)")
                case .JSONParsingError(let message):
                    self.goToLogin(withErrorString: message)
                default:
                    self.goToLogin(withErrorString: "Error occured during logout")
                }
                
                return
            }
            
            self.goToLogin(withErrorString: nil)
        }
    }
    
    
    private func goToLogin(withErrorString text: String?) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    private func isLoading(_ state: Bool) {
        if state {
            DispatchQueue.main.async {
                self.loadingView.isHidden = !state
                UIView.animate(withDuration: 0.35, animations: {
                    self.loadingView.alpha = 1.0
                })
            }
        } else {
            DispatchQueue.main.async {
                self.loadingView.isHidden = state
                UIView.animate(withDuration: 0.35, animations: {
                    self.loadingView.alpha = 0.0
                })
            }
        }
    }
    
    
    private func updateLoadingText(with text: String) {
        DispatchQueue.main.async {
            self.loadingText.text = text
        }
    }

}
