//
//  UpdateMediaUrlViewController.swift
//  OnTheMap
//
//  Created by Kyle Price on 5/9/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//

import UIKit
import MapKit

class UpdateMediaUrlViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldWidth: NSLayoutConstraint!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var submitBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var mapViewHeight: NSLayoutConstraint!
    @IBOutlet weak var loadingBlackout: UIView!
    
    var placemark: CLPlacemark!
    
    var screenWidth: CGFloat = 0
    var screenHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUi()
        zoomMapToPlacemark()
        urlTextField.delegate = self
    }
    
    
    @objc func cancelPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func submitPressed(_ sender: Any) {
        if let mediaUrl = urlTextField.text, URL(string: mediaUrl) != nil {
            DataModel.studentData[DataModel.curStudentIdx!].mediaURL = mediaUrl
            isLoading(true)

            ParseClient.shared.postUpdatedStudentLocation(for: DataModel.studentData[DataModel.curStudentIdx!]) { (loc, error) in
                if let error = error as? ParseClient.ParseError {
                    print(error)
                }   // TODO: ADD UI UPDATES FOR ERRORS / PROPER ERROR HANDLING
                
                self.isLoading(false)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    private func setupUi() {
        screenWidth = view.frame.width
        screenHeight = view.frame.height
        
        textFieldWidth.constant = screenWidth * 0.75
        submitBtnWidth.constant = screenWidth * 0.375
        mapViewHeight.constant = screenHeight * 0.7
        
        submitBtn.layer.borderColor = submitBtn.tintColor.cgColor
        submitBtn.layer.borderWidth = 2
        submitBtn.layer.cornerRadius = 10
        
        loadingBlackout.isHidden = true
        loadingBlackout.alpha = 0
        
        let cancelItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        navigationItem.setRightBarButton(cancelItem, animated: true)
        navigationItem.title = "URL"
    }
    
    
    private func zoomMapToPlacemark() {
        let locAnnotation = MKPointAnnotation()
        
        if let locCoordinate = placemark.location?.coordinate {
            locAnnotation.coordinate = locCoordinate
            mapView.showAnnotations([locAnnotation], animated: true)
        } else {
            print("Unable to create annotation and move map")
        }
    }
    
    
    private func disableBtn(_ state: Bool) {
        submitBtn.isEnabled = false
        submitBtn.layer.borderColor = submitBtn.tintColor.cgColor
    }
    
    
    // Clear the keyboard when the user taps the screen
    @IBAction func screenTapped(_ sender: Any) {
        urlTextField.resignFirstResponder()
    }
    
    // MARK: TextField delegate methods
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    // Display the dark blur with loading animated icon
    // or remove it from display
    private func isLoading(_ loading: Bool) {
        DispatchQueue.main.async {
            if loading {
                UIView.animate(withDuration: 0.35, animations: {
                    self.loadingBlackout.isHidden = !loading
                    self.loadingBlackout.alpha = 1
                })
            } else {
                UIView.animate(withDuration: 0.35, animations: {
                    self.loadingBlackout.alpha = 0
                    self.loadingBlackout.isHidden = !loading
                })
            }
        }
    }
}
