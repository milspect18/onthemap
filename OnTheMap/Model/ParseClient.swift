//
//  ParseClient.swift
//  OnTheMap
//
//  Created by Kyle Price on 4/21/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//
//  This is a client class for accessing and manipulating the
//  Parse API server that Udacity is hosting.
//
//

import Foundation

class ParseClient {
    
    // MARK: Properties
    private let host = "parse.udacity.com"
    private let path = "/parse/classes"
    private let studentLoc = "/StudentLocation"
    private let whereKey = "where"
    private let https = "HTTPS"
    private let studentLocLimit = "limit"
    private let studentLocSkip = "skip"
    private let studentLocOrder = "order"
    private let studentLocLimitDefault = 100
    
    // MARK: Singleton instance
    static let shared = ParseClient()
    
    // MARK: Methods
    // Reach out to the Udacity Parse API and give back an array of
    // all student locations
    func getAllStudentLocations(limit: Int?, skip: Int?, order: String?, completion: @escaping (_ locations: [StudentLocation]?, _ error: Error?) -> Void) {
        // Parameters for StudentLocation search
        var params = [
            studentLocLimit : (limit ?? studentLocLimitDefault) as AnyObject,
            studentLocSkip : (skip ?? 0) as AnyObject,
        ]
        
        // If there is an order specified use, otherwise no order
        if let order = order {
            params[studentLocOrder] = order as AnyObject
        }
        
        // GET is the default method, so nil can be passed to the methodType param
        makeParseRequest(with: params, at: studentLoc, body: nil, methodType: nil) { (json, error) in
            if let error = error {
                completion(nil, ParseError.UnknownError(error))   // Give the error back to the caller
            }
            // If error is nil there must be valid json data
            guard   // Decode the JSON into an array of StudentLocation objects
                let studentLocations = try? JSONDecoder().decode(StudentLocArray.self, from: json!)
            else {
                completion(nil, ParseError.JSONParsingError("Error getting students")) // Couldnt decode....
                return
            }
            // Decoded successfully, hand the [StudentLocation] back
            completion(studentLocations.results, nil)
        }
    }
    
    
    // Get single student location
    func getSingleStudentLocation(with searchString: String, completion: @escaping (_ location: [StudentLocation]?, _ error: Error?) -> Void) {
        let params = [whereKey : searchString as AnyObject]
        
        makeParseRequest(with: params, at: studentLoc, body: nil, methodType: nil) { (json, error) in
            // Validity test, parse, callback and pass
            if let error = error {
                completion(nil, ParseError.UnknownError(error))   // Give the error back to the caller
            }
            
            guard   // Decode the JSON into an array of StudentLocation objects
                let studentLocations = try? JSONDecoder().decode(StudentLocArray.self, from: json!)
            else {
                completion(nil, ParseError.JSONParsingError("Error getting student")) // Couldnt decode....
                return
            }
            // Decoded successfully, hand the [StudentLocation] back if it contains results
            if studentLocations.results.isEmpty {
                completion(nil, ParseError.NoResults("No results found"))
            } else {
                completion(studentLocations.results, nil)
            }
        }
    }
    
    
    // POST a new student location
    func postNewStudentLocation(for student: StudentLocation, completion: @escaping (_ updatedStudent: StudentLocation?, _ error: Error?) -> Void) {
        var newStudentLoc = student
        
        guard
            let jsonBody = try? JSONEncoder().encode(student)
        else {
            completion(nil, ParseError.JSONBodyCreationError("Unable to save student data"))
            return
        }
        
        makeParseRequest(with: nil, at: studentLoc, body: jsonBody, methodType: "POST") { (data, error) in
            if let error = error {
                completion(nil, ParseError.UnknownError(error))
            }
            
            guard   // Parse the data as JSON, then grab the returned variables
                let json = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:AnyObject],
                let createdStr = json?["createdAt"] as? String,
                let objectIdStr = json?["objectId"] as? String
            else {  // Returned data was not able to be parsed
                completion(nil, ParseError.JSONParsingError("Unable to save student data"))
                return
            }
            
            newStudentLoc.createdAt = createdStr
            newStudentLoc.objectId = objectIdStr
            completion(newStudentLoc, nil)    // Send the updated StudentLocation back
        }
    }
    
    
    // PUT updated student location
    func postUpdatedStudentLocation(for student: StudentLocation, completion: @escaping (_ updatedStudent: StudentLocation?, _ error: Error?) -> Void) {
        var newStudentLoc = student
        
        guard
            let jsonBody = try? JSONEncoder().encode(student),
            let id = student.objectId
        else {
            completion(nil, ParseError.JSONBodyCreationError("Unable to update student data"))
            return
        }
        
        makeParseRequest(with: nil, at: "\(studentLoc)/\(id)", body: jsonBody, methodType: "PUT") { (data, error) in
            if let error = error {
                completion(nil, ParseError.UnknownError(error))
            }
            
            guard   // Parse the data as JSON, then grab the returned variable
                let json = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:String],
                let updatedAtStr = json?["updatedAt"]
            else {  // Returned data was not able to be parsed
                completion(nil, ParseError.JSONParsingError("Unable to update student data"))
                return
            }
            
            newStudentLoc.updatedAt = updatedAtStr
            completion(newStudentLoc, nil)
        }
    }
    
    
    // Using passed in URL parameters, generate a valid URL for the Udacity Parse API
    private func makeParseUrlFromParameters(parameters: [String:AnyObject]?, pathExtension: String?) -> URL? {
        var components = URLComponents()
        components.scheme = https
        components.host = host
        components.path = path + (pathExtension ?? "")
        
        if parameters != nil {
            components.queryItems = [URLQueryItem]()
            
            for (key, value) in parameters! {
                components.queryItems!.append(URLQueryItem(name: key, value: "\(value)"))
            }
        }
        
        return components.url
    }
    
    
    // Assemble the URL, request and returned data from the request
    // then pass that return data back to the completion handler
    private func makeParseRequest(with params: [String:AnyObject]?, at pathExt: String?, body: Data?, methodType: String?, completion: @escaping (_ json: Data?, _ error: Error?) -> Void) {
        // Generate the url, create the task and send the results to the completion handler
        if let url = makeParseUrlFromParameters(parameters: params, pathExtension: pathExt) {
            var request = URLRequest(url: url)
            
            request.addValue("QrX47CA9cyuGewLdsL7o5Eb8iug6Em8ye0dnAbIr", forHTTPHeaderField: "X-Parse-Application-Id")
            request.addValue("QuWThTdiRmTux3YaDseUSEpUKo7aBYM737yKd4gY", forHTTPHeaderField: "X-Parse-REST-API-Key")
            
            if body != nil {    // If valid body data is passed it must be a post
                request.httpMethod = methodType
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpBody = body
            }
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                completion(data, error)
            }
            
            task.resume()
        } else {
            completion(nil, ParseClient.URLBuildError())
        }
    }
    
}



// Add custom error types for use in returning issues to completion handlers
extension ParseClient {
    struct URLBuildError: Error {
        let localizedDescription = "Unable to produce a valid Udacity Parse URL"
    }

    enum ParseError: Error {
        case NoResults(String)
        case JSONParsingError(String)
        case JSONBodyCreationError(String)
        case URLBuildError(String)
        case UnknownError(Error)
    }
}
