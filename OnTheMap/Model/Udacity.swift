//
//  UdacitySession.swift
//  OnTheMap
//
//  Created by Kyle Price on 4/27/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//
//  These are convenience types to map JSON data from the
//  Udacity auth flow into memory objects.
//

import Foundation

struct UdacitySession: Codable {
    var account: Account
    var session: Session
}

struct Account: Codable {
    var registered: Bool?
    var key: String?
}

struct Session: Codable {
    var id: String?
    var expiration: String?
}

struct BadLogin: Codable {
    var status: Int
    var error: String
}
