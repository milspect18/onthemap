//
//  DataModel.swift
//  OnTheMap
//
//  Created by Kyle Price on 5/15/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//


class DataModel {
    static var studentData = [StudentLocation]()
    static var curStudentIdx: Int?
    static var udacityFirstName = ""
    static var udacityLastName = ""
}
