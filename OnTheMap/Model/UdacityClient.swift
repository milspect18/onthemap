//
//  UdacityClient.swift
//  OnTheMap
//
//  Created by Kyle Price on 4/27/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//

import Foundation

class UdacityClient {
    
    // MARK: Properties
    private let host = "www.udacity.com"
    private let sessionPath = "/api/session"
    private let userPath = "/api/users"
    private let https = "HTTPS"
    private let udacity = "udacity"
    private let uname = "username"
    private let pword = "password"
    private var currentSession: UdacitySession!
    
    // MARK: Singleton instance
    static let shared = UdacityClient()
    
    
    // MARK: Methods
    func authenticateSession(username: String, password: String, completion: @escaping (_ error: Error?) -> Void) {
        let dict: [String:[String:String]] = [udacity:[uname:username, pword:password]]
        
        guard
            let jsonBody = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
        else {
            completion(UdacityError.JSONBodyCreationError("Error during authentication"))
            return
        }
        
        makeUdacityPostRequest(with: nil, at: nil, body: jsonBody) { (json, error) in
            if let error = error {
                completion(UdacityError.UdacitySessionGenerationError(error.localizedDescription))
                return
            }
            
            // Test to see if the credentials given were bogus
            if let badLogin = try? JSONDecoder().decode(BadLogin.self, from: json!) {
                completion(UdacityError.BadLoginError(badLogin.error))
                return
            }
            
            guard
                let session = try? JSONDecoder().decode(UdacitySession.self, from: json!)
            else {
                completion(UdacityError.JSONParsingError("Error during authentication"))
                return
            }
            
            self.currentSession = session
            completion(nil)
        }
    }
    
    
    // DELETE an existing UdacitySession via the Udacity API to logout
    func logoutOfCurrentSession(completion: @escaping (_ error: Error?) -> Void) {
        makeUdacityDeleteRequest { (json, error) in
            if let error = error {
                completion(UdacityError.UnknownError(error))
                return
            }
            
            guard
                let sessionDetails = try? JSONSerialization.jsonObject(with: json!, options: .allowFragments) as? [String:AnyObject],
                let sess = sessionDetails?["session"] as? [String:String],
                let exp = sess["expiration"],
                let id = sess["id"]
            else {
                completion(UdacityError.JSONParsingError("Error logging out"))
                return
            }
            
            self.currentSession.session.expiration = exp
            self.currentSession.session.id = id
            completion(nil)
        }
    }
    
    
    // Get Udacity users first and last name
    func getUserFullName(withKey: String, completion: @escaping (_ first: String?, _ last: String?, _ key: String?, _ error: Error?) -> Void) {
        let getPath = "\(userPath)/\(withKey)"
        
        makeUdacityGetRequest(at: getPath) { (json, error) in
            if let error = error {
                completion(nil, nil, nil, error)
            }
            
            guard
                let results = try? JSONSerialization.jsonObject(with: json!, options: .allowFragments) as? [String:AnyObject],
                let user = results?["user"] as? [String:AnyObject],
                let first = user["first_name"] as? String,
                let last = user["last_name"] as? String,
                let key = user["key"] as? String
            else {
                completion(nil, nil, nil, UdacityError.JSONParsingError("Unable to get student data"))
                return
            }
            
            completion(first, last, key, nil)
        }
    }
    
    
    // Assemble the URL, request and returned data from the POST request
    // then pass that return data back to the completion handler
    private func makeUdacityPostRequest(with params: [String:AnyObject]?, at pathExt: String?, body: Data, completion: @escaping (_ json: Data?, _ error: Error?) -> Void) {
        // Generate the url, create the task and send the results to the completion handler
        let path = sessionPath + (pathExt ?? "")
        
        if let url = makeUdacityUrlFromParameters(parameters: params, path: path) {
            var request = URLRequest(url: url)
            
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpMethod = "POST"
            request.httpBody = body
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if error != nil {
                    completion(data, error)
                    return
                }
                
                let parsibleData = data!.subdata(in: Range(5..<data!.count))
                completion(parsibleData, error)
            }
            
            task.resume()
        } else {
            completion(nil, UdacityError.URLBuildError("Error occured when saving user data"))
        }
    }
    
    
    // Assemble the URL, create and launch the data task for that URL request
    private func makeUdacityDeleteRequest(completion: @escaping (_ json: Data?, _ error: Error?) -> Void) {
        // Generate the url, create the task and send the results to the completion handler
        if let url = makeUdacityUrlFromParameters(parameters: nil, path: sessionPath) {
            var request = URLRequest(url: url)
            var xsrfCookie: HTTPCookie? = nil

            request.httpMethod = "DELETE"
            
            for cookie in HTTPCookieStorage.shared.cookies! {
                if cookie.name == "XSRF-TOKEN" {
                    xsrfCookie = cookie
                    request.setValue(xsrfCookie!.value, forHTTPHeaderField: "X-XSRF-TOKEN")
                    break
                }
            }
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if error != nil {
                    completion(data, error)
                    return
                }
                
                let parsibleData = data?.subdata(in: Range(5..<data!.count))
                completion(parsibleData, error)
            }
            
            task.resume()
        } else {
            completion(nil, UdacityError.URLBuildError("Error occured when deleting user data"))
        }
    }
    
    
    private func makeUdacityGetRequest(at path: String, completion: @escaping (_ json: Data?, _ error: Error?) -> Void) {
        if let url = makeUdacityUrlFromParameters(parameters: nil, path: path) {
            let request = URLRequest(url: url)
            
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    completion(nil, error)
                    return
                }
                
                let parsibleData = data?.subdata(in: Range(5..<data!.count))
                completion(parsibleData, error)
            }
            
            task.resume()
        } else {
            completion(nil, UdacityError.URLBuildError("Error occured when getting user data"))
        }
    }
    
    
    // Using passed in URL parameters, generate a valid URL for the Udacity API
    private func makeUdacityUrlFromParameters(parameters: [String:AnyObject]?, path: String) -> URL? {
        var components = URLComponents()
        components.scheme = https
        components.host = host
        components.path = path
        
        if parameters != nil {
            components.queryItems = [URLQueryItem]()
            
            for (key, value) in parameters! {
                components.queryItems!.append(URLQueryItem(name: key, value: "\(value)"))
            }
        }
        
        return components.url
    }
    
    
    // Restricted access to the currentSession
    func getCurrentSession() -> UdacitySession {
        return currentSession
    }
}




extension UdacityClient {
    enum UdacityError: Error {
        case BadLoginError(String)
        case UdacitySessionGenerationError(String)
        case JSONBodyCreationError(String)
        case URLBuildError(String)
        case JSONParsingError(String)
        case UnknownError(Error)
    }
}



