//
//  StudentLocation.swift
//  OnTheMap
//
//  Created by Kyle Price on 4/21/18.
//  Copyright © 2018 Kyle Price Apps. All rights reserved.
//
//  This model data adds a convenient way to encode and decode
//  the API return data into objects within the app
//

import Foundation

// This struct is a representation of the data
// stored in the StudentLocation endpoint of the
// Udacity Parse API 
struct StudentLocation: Codable, Equatable {
    // MARK: Properties
    var objectId: String?
    var uniqueKey: String?
    var firstName: String?
    var lastName: String?
    var mapString: String?
    var latitude: Float?
    var longitude: Float?
    var createdAt: String?
    var updatedAt: String?
    var mediaURL: String?
    
    static var curUser = StudentLocation()
    
    
    enum CodingKeys: String, CodingKey {
        case uniqueKey
        case firstName
        case lastName
        case mapString
        case latitude
        case longitude
        case createdAt
        case updatedAt
        case mediaURL
        case objectId
    }
    
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(uniqueKey, forKey: .uniqueKey)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(mapString, forKey: .mapString)
        try container.encode(latitude, forKey: .latitude)
        try container.encode(longitude, forKey: .longitude)
        try container.encode(mediaURL, forKey: .mediaURL)
    }
    
    static func ==(lhs: StudentLocation, rhs: StudentLocation) -> Bool {
        var ret = false
        
        ret = lhs.firstName?.lowercased() == rhs.firstName?.lowercased()
        ret = ret && lhs.lastName?.lowercased() == rhs.lastName?.lowercased()
        ret = ret && lhs.uniqueKey == rhs.uniqueKey
        
        return ret
    }
}


// The API returns a JSON object with the following format:
//      { "results" : [ {...}, {...}, {...} ] }
// This struct is designed to allow direct codability between the JSON
// and Swift objects.  This will create a struct that has a var that
// maps to an array of StudentLocation objects and instruct the decoder
// how to map that data.
struct StudentLocArray: Codable {
    var results = [StudentLocation]()
    
    enum ArrayKeys: String, CodingKey {
        case results
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ArrayKeys.self)
        var locArrayForType = try container.nestedUnkeyedContainer(forKey: ArrayKeys.results)
        
        while(!locArrayForType.isAtEnd) {
            self.results.append(try locArrayForType.decode(StudentLocation.self))
        }
    }
}

